# Basic Function - Screenshot

### Introduction

 This sample shows how to use the screenshot APIs to take screenshots in the eTS project.

### Usage

1. Touch **Fullscreen snapshot** to take and display a full-screen snapshot.

2. Touch **Fixed-size screenshot** to take and display a fixed-size screenshot.

### Constraints

This sample can only be run on standard-system devices.
