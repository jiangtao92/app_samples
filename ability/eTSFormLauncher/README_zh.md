# 卡片使用方

### 简介

本示例展示了FA模型卡片使用方的创建与使用，包括创建与展示卡片、转换卡片、更新卡片、删除卡片等功能。

### 使用说明

1、启动应用，拉起MainAbility，页面正常显示**eTSFormLauncher**字样。

2、若此时已安装对应的卡片提供方应用，如[FA模型卡片](../eTSFormAbility/README_zh.md)或[Stage模型卡片](../eTSFormExtAbility/README_zh.md)，则会显示出卡片。

<img src="screenshots/eTSFormLauncherExample.png" alt="eTSFormLauncherExample" style="zoom:50%;" />

3、点击**castTempForm**按钮，则会把临时卡片转换为常规卡片。

4、点击**updateForm**按钮，则会更新卡片。

5、点击**deleteForm**按钮，则会删除卡片。

### 约束与限制

1、本示例仅支持标准系统上运行。
2、本示例需要使用3.0.0.900及以上的IDE版本才可编译运行。
