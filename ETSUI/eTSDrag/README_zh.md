#  ArkUI-拖拽事件

### 简介

本示例主要展示了拖拽操作的功能。

### 使用说明

1. 按住桌面图标进行拖拽可以与桌面其他图标交换位置。
2. 按住桌面图标可以拖拽至底部Dock栏任意位置。
3. 按住底部Dock栏图标进行拖拽可以与Dock栏其他图标交换位置。
4. 点击底部Dock栏图标可以进行移除，移除的图标可以返回到桌面上。

### 约束与限制

本示例仅支持在标准系统上运行。