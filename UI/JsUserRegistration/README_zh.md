# 用户注册（JS）

### 简介

本示例通过用户注册案例来展示 JS FA 应用基本控件的使用，包括文本输入框，日期选择控件，单选按钮，下拉菜单和普通按钮等。

### 使用说明

按要求输入姓名，选择出生日期、性别、学历，点击**register**按钮，提示success注册成功

### 约束与限制

本示例仅支持在标准系统上运行。